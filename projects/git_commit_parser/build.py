import os
from pybuilder.core import depends, init, task, use_plugin

use_plugin("python.core")
use_plugin("python.unittest")
use_plugin("python.install_dependencies")
use_plugin("python.distutils")
use_plugin('pypi:pybuilder_aws_plugin')

version = "0.0.1"
default_task = ["publish"]


@init
def initialize(project):
    project.depends_on_requirements("requirements.txt")
    project.set_property('bucket_name', os.environ['S3_BUCKET'])
    project.set_property('bucket_prefix', 'dev/git_commit_parser/')

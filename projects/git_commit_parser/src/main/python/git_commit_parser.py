'''
This script looks at the status of an item in the changelist of the commit, and returns that along with the filename in
a dictionary.
TODO: Fix the reverse diff issue someday
'''
from argparse import ArgumentParser
from git import Repo

# Constants
EMPTY_TREE_SHA = "4b825dc642cb6eb9a060e54bf8d69288fbee4904"


def get_type(diff):
    """Return the change type of this diff."""
    # The logic for addition and deletion appears to be swapped, because this is showing the diff in reverse. Trust me.
    if diff.renamed:
        return 'R'
    if diff.deleted_file:
        return 'A'
    if diff.new_file:
        return 'D'
    return 'M'


def parse_commit(commit, root_folder_path=""):
    """
    Parse the given commit by looking at the diffs and checking the files that changed.
    If applicable, the file to change_list then return the option when complete.

    :param commit: The commit to parse.
    :param root_folder_path: (optional) Path to the root folder from which you want to parse for changes.
    :return: A dictionary containing file paths and the change type
    """
    parent = commit.parents[0] if commit.parents else EMPTY_TREE_SHA
    diffs = {
        diff.a_path: diff for diff in commit.diff(parent)
    }

    # Loop through every file in this commit and process it
    change_list = {}

    for filename, attrs in commit.stats.files.items():
        diff = diffs.get(filename)

        # We need to check for None here because a rename won't have a diff
        if diff:
            change_type = get_type(diff)
            file_path = '/'.join(filename.split('/')[:-1])

            if root_folder_path == "" or (root_folder_path != "" and root_folder_path in file_path):
                change_list[filename] = change_type

    return change_list


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-r", "--repo_path", dest="repo_path", required=True)
    args = parser.parse_args()
    repo = Repo(args.repo_path)

    if not repo.bare:
        print('Repo loaded at {}, parsing last commit...'.format(args.repo_path))
        commit = repo.head.commit
        changes = parse_commit(commit)
        print(str(changes))
        print('Finished parsing commits')
    else:
        print('Error loading repository at {}'.format(args.repo_path))

import os
from git import Repo
from git_commit_parser import parse_commit

current_dir = os.getcwd()
repo = Repo(current_dir)


def return_filename():
    if not repo.bare:
        commit = repo.head.commit
        changes = parse_commit(commit)
        file_path = next(iter(changes))
        package_folder = ''
        try:
            package_folder = file_path.split('/')[1]
        except IndexError as e:
            # Can't display anything here, as the upstream caller is relying on stdout
            pass

        print(package_folder)


if __name__ == "__main__":
    return_filename()

"""
Unit tests for the git_commit_parser script.
"""
import os
import pathlib
import shutil
import sys
import unittest
from git import Repo

sys.path.append('projects/git_commit_parser/src/main/python')
import git_commit_parser


class GitCommitParserTests(unittest.TestCase):
    """
    Unit test class (TestCase);
    """

    def setUp(self):
        self.repo_path = '/tmp/git_commit_parser'
        self.bare_repo = Repo.init(self.repo_path)

    def tearDown(self):
        shutil.rmtree(str(self.bare_repo.working_tree_dir))

    def test_should_return_single_change(self):
        index = self.bare_repo.index
        new_file_path = os.path.join(self.bare_repo.working_tree_dir, 'test.txt')
        open(new_file_path, 'w').close()
        index.add([new_file_path])
        index.commit("test commit")

        change_list = git_commit_parser.parse_commit(self.bare_repo.head.commit)
        print("change list: " + str(change_list))
        assert change_list == {'test.txt': 'A'}

    def test_should_return_single_change_path(self):
        index = self.bare_repo.index
        project_path = 'projects/test_project'
        pathlib.Path(self.bare_repo.working_tree_dir + f'/{project_path}').mkdir(parents=True, exist_ok=True)
        new_file_path = os.path.join(self.bare_repo.working_tree_dir, f"{project_path}/test.txt")
        open(new_file_path, 'w').close()
        index.add([new_file_path])
        index.commit("test commit")

        change_list = git_commit_parser.parse_commit(self.bare_repo.head.commit, root_folder_path='projects')
        expected_change_list = {f"{project_path}/test.txt": 'A'}
        print("change list: " + str(change_list))
        assert change_list == expected_change_list

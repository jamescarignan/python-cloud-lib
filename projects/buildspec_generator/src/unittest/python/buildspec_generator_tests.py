import unittest
import sys

sys.path.append('projects/buildspec_generator/src/main/python')
from buildspec_generator.buildspec_generator import BuildspecGenerator
from buildspec_generator.defaults import BUILDSPEC_VERSION


class BuildspecGeneratorTests(unittest.TestCase):
    maxDiff = None

    def setUp(self):
        self.generator = BuildspecGenerator()
        self.expected_template = {
          "version": BUILDSPEC_VERSION,
          "phases": {}
        }

    def test_add_single_phase(self):
        """
        Test adding a single phase to a template
        """
        install_config = {
            "commands": [
                "command1",
                "command2"
            ]
        }
        self.generator.update_phase("install", install_config)
        self.expected_template["phases"]["install"] = install_config
        self.assertEqual(self.generator.template, self.expected_template)

    def test_add_single_phase_runtime_versions(self):
        """
        Test adding an install phase with runtime-versions config
        """
        install_config = {
            "runtime-versions": {
                "android": "28"
            }
        }
        self.generator.update_phase("install", install_config)
        self.expected_template["phases"]["install"] = install_config
        self.assertEqual(self.generator.template, self.expected_template)

    def test_add_single_phase_runtime_versions_invalid(self):
        """
        Test adding an non-install phase with runtime-versions config
        """
        build_config = {
            "runtime-versions": {
                "android": "28"
            }
        }
        with self.assertRaises(ValueError) as context:
            self.generator.update_phase("build", build_config)

        self.assertTrue("Phase subtype 'runtime-versions' can only be used for phase type 'install'." in str(context.exception))

    def test_add_invalid_phase(self):
        """
        Test adding an invalid phase
        """
        some_config = {
            "something": {
                "key": "value"
            }
        }
        with self.assertRaises(ValueError) as context:
            self.generator.update_phase("test", some_config)

        self.assertTrue("Phase_type must be in " in str(context.exception))

    def test_add_invalid_phase_subtype(self):
        """
        Test adding an invalid phase subtype
        """
        build_config = {
            "invalid": {
                "key": "value"
            }
        }
        with self.assertRaises(ValueError) as context:
            self.generator.update_phase("build", build_config)

        self.assertTrue("Top-level key in phase config must be in " in str(context.exception))

    def test_update_artifacts(self):
        """
        Test updating the artifacts block
        """
        self.generator.update_artifacts(['hello_world.txt'])
        self.expected_template["artifacts"] = {}
        self.expected_template["artifacts"]["files"] = ['hello_world.txt']
        self.expected_template["artifacts"]["discard-paths"] = 'yes'
        self.assertEqual(self.generator.template, self.expected_template)

    def test_update_artifacts_optional_arguments(self):
        """
        Test updating the artifacts block
        """
        secondary_artifacts = {
            "secondary-artifact": {
                'files': ['testing.txt'],
                'discard-paths': 'yes',
                'name': 'test2.zip',
                'base-directory': '/home/username'
            }
        }
        self.generator.update_artifacts(['hello_world.txt'], name='test.zip', discard_paths='no', base_directory='/tmp', secondary_artifacts=secondary_artifacts)
        self.expected_template["artifacts"] = {}
        self.expected_template["artifacts"]["files"] = ['hello_world.txt']
        self.expected_template["artifacts"]["discard-paths"] = 'no'
        self.expected_template["artifacts"]["name"] = 'test.zip'
        self.expected_template["artifacts"]["base-directory"] = '/tmp'
        self.expected_template["artifacts"]["secondary-artifacts"] = secondary_artifacts
        self.assertEqual(self.generator.template, self.expected_template)

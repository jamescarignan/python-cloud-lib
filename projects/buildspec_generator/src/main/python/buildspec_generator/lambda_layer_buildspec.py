from .buildspec_generator import BuildspecGenerator


class LambdaLayerBuildspecGenerator(BuildspecGenerator):
    """
    Generates a buildspec file for a Lambda Layer pipeline
    """
    def __init__(self):
        """
        Build the template on initialization.
        """
        super().__init__()
        self.build_template()

    def build_template(self):
        """
        Build the template.
        """
        self.update_phase("install", {
            "commands": [
                "echo Build started on `date`",
                "pip install --upgrade pip"
            ]
        })
        self.update_phase("build", {
            "commands": [
                'echo "Packaging Lambda requirements..."',
                "mkdir -p temp/python",
                "pip install -r requirements.txt -t python",
                "echo Finished!"
            ]
        })
        self.update_phase("post_build", {
            "commands": [
                "echo Build completed on `date`"
            ]
        })
        self.update_artifacts(['python/**/*'], discard_paths='yes')

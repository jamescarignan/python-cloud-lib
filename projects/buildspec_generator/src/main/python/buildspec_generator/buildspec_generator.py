from ruamel.yaml import YAML
from .defaults import BUILDSPEC_VERSION
from .constants import PHASE_TYPES, PHASE_SUBTYPES


class BuildspecGenerator:
    def __init__(self):
        self.template = {
            "version": BUILDSPEC_VERSION,
            "phases": {}
        }

    def update_run_as(self):
        raise NotImplementedError("Not implemented yet.")

    def update_env(self):
        raise NotImplementedError("Not implemented yet.")

    def update_phase(self, phase_type, config):
        """
        Update a phase in the template.
        """
        if phase_type not in PHASE_TYPES:
            raise ValueError(f"Phase_type must be in {str(PHASE_TYPES)}")
        if phase_type != "install" and "runtime-versions" in config.keys():
            raise ValueError("Phase subtype 'runtime-versions' can only be used for phase type 'install'.")
        for key, value in config.items():
            if key not in PHASE_SUBTYPES:
                raise ValueError(f"Top-level key in phase config must be in {str(PHASE_SUBTYPES)}")
        self.template["phases"][phase_type] = config

    def update_artifacts(self, files, name=None, discard_paths='yes', base_directory=None, secondary_artifacts=None):
        """
        Update the artifacts block of a template.
        """
        if 'artifacts' not in self.template.keys():
            self.template['artifacts'] = {}
        artifact_config = {
            "files": files,
            "discard-paths": discard_paths
        }
        if name is not None:
            artifact_config["name"] = name
        if base_directory is not None:
            artifact_config["base-directory"] = base_directory
        if secondary_artifacts is not None:
            artifact_config["secondary-artifacts"] = secondary_artifacts
        self.template['artifacts'] = artifact_config

    def update_cache(self):
        raise NotImplementedError("Not implemented yet.")

    def build_template(self):
        """
        Method to build the template with desired values. Must be implemented in child class.
        """
        raise NotImplementedError("You must implement build_template in your child class.")

    def render_template(self, path='buildspec.yml'):
        """
        Render the template to YAML.
        """
        if 'phases' not in self.template.keys():
            raise ValueError("You must add at least one phase to the template before rendering.")
        yaml = YAML()
        yaml.default_flow_style = False
        with open(path, 'w') as outfile:
            file_contents = yaml.dump(self.template, outfile)


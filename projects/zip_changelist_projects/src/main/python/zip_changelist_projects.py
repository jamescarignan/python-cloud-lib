from shutil import copyfile, rmtree
import json
import os
import traceback
import zipfile
import boto3
from git import Repo
import git_commit_parser
from buildspec_generator.lambda_layer_buildspec import LambdaLayerBuildspecGenerator

try:
    import zlib
    compression = zipfile.ZIP_DEFLATED
except:
    compression = zipfile.ZIP_STORED

modes = {zipfile.ZIP_DEFLATED: 'deflated',
         zipfile.ZIP_STORED:   'stored',
         }


def get_changelist(codecommit_repo, repo_name):
    '''
    This function is the entry point of the Lambda. It takes in a CodeCommit repository ARN, clones the master branch, and parses out the changelist. Last, it zips each project's files and uploads them to S3.
    :param repo_arn: ARN of the CodeCommit repo to be cloned.
    :return: A dictionary containing file paths and the change type
    '''
    repo_path = f"/tmp/{repo_name}"
    if os.path.isdir(repo_path):
        rmtree(repo_path)
    base_clone_url_https = codecommit_repo['repositoryMetadata']['cloneUrlHttp']
    # Copy the git askpass script to /tmp and make it executable
    dir_path = os.path.dirname(os.path.realpath(__file__))
    copyfile('{}/ask_pass.py'.format(dir_path), '/tmp/ask_pass.py')
    os.chmod('/tmp/ask_pass.py', 0o777)
    # Clone repo, parse and return changelist
    repo = clone_repo(base_clone_url_https, repo_path)
    change_list = git_commit_parser.parse_commit(repo.head.commit)
    return change_list


def clone_repo(base_clone_url_https, repo_path):
    # Pull git username and password params from SSM
    params = get_ssm_params('/okzun/codecommit_automation_https_username', '/okzun/codecommit_automation_https_password')
    repo = Repo.clone_from(base_clone_url_https, repo_path, env={
        'GIT_USERNAME': params['username'],
        'GIT_PASSWORD': params['password'],
        'GIT_ASKPASS': '/tmp/ask_pass.py'
    })
    return repo


def zip_changelist_projects(change_list, repo_name):
    # Loop through changelist, add unique project names, zip them up and upload to S3
    projects = []
    for file, change_type in change_list.items():
        project_dir = file.split('/')[1]
        # Only add to list if not already present, and is an addition or modification
        # Skipping deletes for now, though this might be wrong!
        if project_dir not in projects and change_type in ['A', 'M']:
            projects.append(project_dir)

    file_list = []
    for project in projects:
        file_path = f'/tmp/{project}.zip'
        with zipfile.ZipFile(file_path, 'w') as zf:
            project_path = f'/tmp/{repo_name}/projects/{project}'
            create_buildspec_file(project_path)
            for folder, subfolders, files in os.walk(project_path):
                for file in files:
                    zf.write(os.path.join(folder, file), arcname=file, compress_type=compression)
        file_list.append(file_path)
    return file_list


def create_buildspec_file(project_path):
    """
    Broken out to a separate method to mock for tests.
    """
    buildspec_file = LambdaLayerBuildspecGenerator()
    buildspec_file.render_template(f'{project_path}/buildspec.yml')


def upload_zips_to_s3(zip_list):
    s3_client = boto3.client('s3')
    responses = []
    for zip_file in zip_list:
        file_name = zip_file.split('/')[2]
        response = s3_client.upload_file(zip_file, os.environ['S3_BUCKET'], 'source/{}'.format(file_name))
        responses.append(response)
    return responses


def get_ssm_params(username_param, password_param):
    ssm_client = boto3.client('ssm')
    https_username_param = ssm_client.get_parameter(Name=username_param, WithDecryption=True)
    https_username = https_username_param['Parameter']['Value']
    https_password_param = ssm_client.get_parameter(Name=password_param, WithDecryption=True)
    https_password = https_password_param['Parameter']['Value']
    return {'username': https_username, 'password': https_password}


def get_codecommit_repo(repo_name):
    # Get the CodeCommit repo resource in order to find the https clone URL
    codecommit_client = boto3.client('codecommit')
    return codecommit_client.get_repository(repositoryName=repo_name)


def handler(event, context):
    print("event: " + str(event))
    print("testing")

    # Create response object - assume an error
    response = {
        "statusCode": 400,
        "headers": {'Content-Type': 'application/json'},
        "body": {
            "message": "Error uploading projects to S3.",
            "upload_responses": []
        }
    }

    try:
        repo_arn = event['Records'][0]['eventSourceARN']
        repo_name = repo_arn.split(':')[5]
        print(f"repo name: {repo_name}")
        codecommit_repo = get_codecommit_repo(repo_name)
        change_list = get_changelist(codecommit_repo, repo_name)
        zip_list = zip_changelist_projects(change_list, repo_name)
        print("zip_list: " + str(zip_list))
        upload_responses = upload_zips_to_s3(zip_list)
        print("upload_responses: " + str(upload_responses))
        response["statusCode"] = 200
        response["body"] = {
                "message": "Projects uploaded to S3.",
                "upload_responses": upload_responses
        }
    except Exception as e:
        print("Error uploading projects to S3: {}".format(e))
        traceback.print_exc()
    response["body"] = json.dumps(response["body"])
    return response

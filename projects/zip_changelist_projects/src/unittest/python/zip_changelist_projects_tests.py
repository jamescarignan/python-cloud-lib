"""
Unit tests for the zip_changelist_projects script.
"""
import json
import os
import shutil
import sys
import unittest
from mock import patch
import boto3
from git import Repo

sys.path.append('src/main/python')
import zip_changelist_projects

codecommit_repo = {
        'repositoryMetadata': {
            'cloneUrlHttp': 'https://repo.hosting.com'
        }}

ssm_params = {'username': 'username', 'password': 'password'}
upload_responses = []


class ZipChangelistProjectsTests(unittest.TestCase):
    """
    Unit test class (TestCase);
    """

    def setUp(self):
        self.commit_id = "bf3557bbdd0bafd93c831c96d618f547d738867g"
        self.repo_path = '/tmp/zip_changelist_projects'
        self.bare_repo = Repo.init(self.repo_path)
        self.event = {
            "Records": [{
                "eventSourceARN": "arn:aws:codecommit:us-west-2:123456789012:test-repo"
            }]
        }

    def tearDown(self):
        shutil.rmtree(str(self.bare_repo.working_tree_dir))

    @patch.object(boto3, 'client', autospec=True)
    @patch.object(zip_changelist_projects, 'get_codecommit_repo', return_value=codecommit_repo)
    @patch.object(zip_changelist_projects, 'get_ssm_params', return_value=ssm_params)
    @patch.object(zip_changelist_projects, 'upload_zips_to_s3', return_value=upload_responses)
    @patch.object(zip_changelist_projects, 'create_buildspec_file', autospec=True)
    def test_should_return_single_change(self, mock_client, mock_repo, mock_params, mock_upload_response, mock_buildspec):
        # We need to initialize a repo and get a single change in there
        index = self.bare_repo.index
        os.mkdir(self.bare_repo.working_tree_dir + '/projects')
        os.mkdir(self.bare_repo.working_tree_dir + '/projects/test-project')
        print("git root: " + str(self.bare_repo.working_tree_dir))
        new_file_path = os.path.join(self.bare_repo.working_tree_dir, 'projects/test-project/test.txt')
        open(new_file_path, 'wb').close()
        index.add([new_file_path])
        index.commit("test commit")

        with patch.dict('os.environ', {'S3_BUCKET': 'test-bucket'}):
            with patch.object(zip_changelist_projects, 'clone_repo', return_value=self.bare_repo):
                response = zip_changelist_projects.handler(self.event, None)
                response_body = json.loads(response["body"])
                assert response["statusCode"] == 200
                assert response_body["message"] == "Projects uploaded to S3."
                assert response_body["upload_responses"] == []

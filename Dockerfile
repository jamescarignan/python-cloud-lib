FROM ubuntu:18.04

# Update package repo and install necessary bootstrap stuffs
RUN apt-get update -qy \
  && apt-get install -y python3-pip python3-dev git-core \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

# Copy required files and set WORKDIR
RUN mkdir /build
COPY /projects/git_commit_parser /build
WORKDIR /build
RUN pip install -r requirements-dev.txt

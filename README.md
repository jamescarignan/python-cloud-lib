# python-cloud-lib

A collection of Python packages and functions to help with cloud engineering tasks.

## Gitlab environment variables
Set these in order for the pipeline to work.
* **AWS_ACCESS_KEY_ID**: The AWS_ACCESS_KEY_ID of the IAM user being used
* **AWS_SECRET_ACCESS_KEY**: The AWS_SECRET_ACCESS_KEY of the IAM user being used
* **S3_BUCKET**: The S3 bucket to upload package artifacts to
* **gitlab_user**: The Gitlab username where the docker image resides(i.e. yours)

## Build docker image
This repo's pipeline uses a custom docker image in order to package up some prereqs and reduce build time. These instructions assume you're forking this repo. First, [enable the container registry for the project](https://docs.gitlab.com/ee/user/project/container_registry.html). Then you'll probably need to log into Gitlab's registry first:

`docker login registry.gitlab.com`

Build it with this:

`docker build -t registry.gitlab.com/<user>/python-cloud-lib/autobuilder .`

And publish it with this:

`docker push registry.gitlab.com/<user>/python-cloud-lib/autobuilder`

## Running tests
From the root directory:

`python -m unittest discover projects.<project_name>.src.unittest.python *_tests.py`

## Publishing to S3
The pipeline will publish a package to S3 automatically if a valid change is detected, but if you want to do so manually:

`cd projects/$PROJECT_NAME`

`pyb upload_zip_to_s3`

## License
[MIT](https://choosealicense.com/licenses/mit/)
